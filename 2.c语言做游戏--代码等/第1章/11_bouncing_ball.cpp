#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

int stationary_ball()
{
	int i,j;
	int x = 5;
	int y = 10;
	//输出小球上面的 空行
	for(i=0; i<x; i++)
		printf("\n");
	//输出小球左边的空格
	for(j=0; j<y; j++) 
		printf(" ");
	printf("o");
	printf("\n"); 
	return 0;
}

int falling_ball()
{
	int i,j;
	int x = 5;
	int y = 10;
	for(x=1; x<10; x++)
	{
		system("cls");
		//输出小球上面的 空行
		for(i=0; i<x; i++)
			printf("\n");
		//输出小球左边的空格
		for(j=0; j<y; j++) 
			printf(" ");
		printf("o");
		printf("\n"); 
	}
	return 0;
}

int updown_ball()
{
	int i,j;
	int x = 5;
	int y = 10;
	int height = 20;
	int velocity = 1;

	while(1)
	{
		x = x + velocity;
		system("cls");

		system("cls");
		//输出小球上面的 空行
		for(i=0; i<x; i++)
			printf("\n");
		//输出小球左边的空格
		for(j=0; j<y; j++) 
			printf(" ");
		printf("o");
		printf("\n"); 
	
		if(x==height)
			velocity = -velocity;
		if(x==0)
			velocity = -velocity;
	}
	return 0;
}

int bouncing_ball()
{
	int i,j;
	int x = 5, y = 10;
	int left = 0, right = 20, top = 0, bottom = 10;
	int velocity_x = 1, velocity_y = 1;

	while(1)
	{
		x = x + velocity_x;
		y = y + velocity_y;
		system("cls");

		system("cls");
		//输出小球上面的 空行
		for(i=0; i<x; i++)
			printf("\n");
		//输出小球左边的空格
		for(j=0; j<y; j++) 
			printf(" ");
		printf("o");
		printf("\n"); 
		Sleep(500);

		if((x==top) || (x==bottom))
			velocity_x = -velocity_x;
		if((y==left) || (y==right))
			velocity_y = -velocity_y;
	}
	return 0;
}

int main()
{
	stationary_ball();
	falling_ball();
	//updown_ball();
	bouncing_ball();

	return 0;
}